# Brick Tower 
It's a block pile up game Developed by Edwin Jones Holanda

### Prerequisites

The project was developed using Unity 2019.4.3f1.
Not tested in previous versions.

### Running

Open the Main scene and hit play, also you can change the input processor at the project hierarchy.

## Built With

* [Unity](https://unity3d.com/) - The game engine used

## Author

* **Edwin Jones Holanda** - *Developer and Artist* - https://edwingamedev.com