﻿namespace EdwinGameDev
{
    public enum InputType
    {
        Up,
        Down,
        Right,
        Left,

        Click,
        Release,
        Hold,
        NoInput
    }
}