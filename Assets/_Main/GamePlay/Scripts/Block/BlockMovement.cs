﻿using System;

namespace EdwinGameDev
{
    [Serializable]
    public struct BlockMovement
    {
        public float horizontal;
        public float vertical;
    }
}