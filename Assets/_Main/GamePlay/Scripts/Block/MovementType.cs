﻿namespace EdwinGameDev
{
    public enum MovementType
    {
        Left,
        Right,
        Down
    }
}