﻿namespace EdwinGameDev
{
    public enum BlockType
    {
        S,
        Z,
        L,
        J,
        T,
        O,
        I
    }
}