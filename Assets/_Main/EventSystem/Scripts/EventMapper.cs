﻿using System;

namespace EdwinGameDev
{
    [Serializable]
    public struct EventMapper
    {
        public string eventName;
        public ScriptableEvent scriptableEvent;
    }
}