﻿namespace EdwinGameDev
{
    public enum StateCommandType
    {
        OpenScene,
        ResumeScene,
        ChangeScene,
        StartGame,
        PauseGame,
        GoToMenu,        
    }
}