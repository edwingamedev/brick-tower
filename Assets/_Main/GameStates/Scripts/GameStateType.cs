﻿namespace EdwinGameDev
{
    public enum GameStateType
    {
        Intro,
        Playing,
        Paused,
        GameOver,
        Win
    }
}